// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
window.axios = require('axios');
Vue.use(VueAxios, axios)
import store from './store';
import VueAxios from 'vue-axios'
import App from './App'
import router from './router'



// Vue.axios.defaults.headers.common = {
//   'X-Requested-With': 'XMLHttpRequest'
// }


Vue.config.productionTip = false

/* eslint-disable no-new */
const app = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
}).$mount('#app')
