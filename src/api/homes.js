import * as puth from './puth'

export default {
    homes (callback, err) {
      const header = { headers: {'authorization': 'Token ' + localStorage.getItem('token')}}
        axios.get(puth.HOMES, header)
            .then(response => {
                callback(response.data)
            }, error => {
              err(error)
            }
          )
            .catch(e => { err(e) } );
    },
}
