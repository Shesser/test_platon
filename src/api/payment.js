import * as puth from './puth'

export default {
    payments (callback, err, data) {
      const header = { headers: {'authorization': 'Token ' + localStorage.getItem('token')}}
      console.log(data)
        axios.post(puth.PAYMENTS, data, header)
            .then(response => {
                callback(response.data)
            }, error => {
              err(error)
            }
          )
            .catch(e => { err(e) } );
    },
}
