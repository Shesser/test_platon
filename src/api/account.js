import * as puth from './puth'

export default {
    getAccount (callback, err, home_id) {
      const header = { headers: {'authorization': 'Token ' + localStorage.getItem('token')}}
        axios.get(puth.ACCOUNT + '?' + 'home_code=' + home_id, header)
            .then(response => {
                callback(response.data)
            }, error => {
              err(error)
            }
          )
            .catch(e => { err(e) } );
    },
}
