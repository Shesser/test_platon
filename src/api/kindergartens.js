import * as puth from './puth'

export default {
    kindergartens (callback, err) {
      const header = { headers: {'authorization': 'Token ' + localStorage.getItem('token')}}
        axios.get(puth.KINDERGARTENS, header)
            .then(response => {
                callback(response.data)
            }, error => {
              err(error)
            }
          )
            .catch(e => { err(e) } );
    },

    kindergartensPayments(callback, err) {
      const header = { headers: {'authorization': 'Token ' + localStorage.getItem('token')}}
        axios.get(puth.KINDERGARTENS_PAYMENTS, header)
            .then(response => {
                console.log('success')
                callback(response.data)
            }, error => {
              console.log("ERROR")
              err(error)
            }
          );
    }
}
