/**
 * Mocking client-server processing
 */
import * as puth from './puth'

export default {
    login (callback, err, form) {
        axios.post(puth.LOGIN, form)
            .then(response => {
                callback(response.data)
            })
            .catch(e => { err(e) } );
    },

    current_user (callback, err) {
      const header = { headers: {'authorization': 'Token ' + localStorage.getItem('token')}}
        axios.get(puth.GET_PROFILE, header)
            .then(response => {
              console.log(response)
                callback(response.data)
            },
            error => {
              err(error)
            }
          );
    },

    logout (callback, err) {
      const header = { headers: {'authorization': 'Token ' + localStorage.getItem('token')}}
        axios.post(puth.LOGOUT, {}, header)
            .then(response => {
                callback(response.data)
            })
            .catch(e => { err(e) } );
    },
}
