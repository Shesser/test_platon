// * -- для аунтифікації --*/
let root = 'http://195.34.206.79/api/v1/'
// let root = 'http://localhost:3000/api/v1/'

export const LOGIN = root + 'users/sign_in'
export const GET_PROFILE = root + 'current_user'
export const LOGOUT = root + 'logout'
export const HOMES = root + 'homes'
export const ACCOUNT = root + 'account'
export const PAYMENTS = root + 'payments'
export const KINDERGARTENS = root + 'kindergartens'
export const KINDERGARTENS_PAYMENTS = root + 'kindergartens_payments'
