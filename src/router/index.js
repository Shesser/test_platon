import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Users from '@/components/Users'
import Homes from '@/components/Homes'
import Payments from '@/components/Payments'
import Kindergartens from '@/components/Kindergartens'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/kindergartens',
      name: 'Kindergartens',
      component: Kindergartens
    },
    {
      path: '/payments',
      name: 'Payments',
      component: Payments
    },
    {
      path: '/homes',
      name: 'Homes',
      component: Homes
    },
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/users',
      name: 'Users',
      component: Users
    }
  ]
})
