import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import homes from './modules/homes'
import account from './modules/account'
import payment from './modules/payment'
import kindergartens from './modules/kindergartens'


Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        user,
        homes,
        account,
        payment,
        kindergartens
    },
})
