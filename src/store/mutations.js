// для auth module
export const AUTORIZATE = 'AUTORIZATE';

// для profile module
export const RECEIVE_USER = 'RECEIVE_PROFILE';
export const DESTROY_PROFILE = 'DESTROY_PROFILE';
export const CURRENT_USER    = 'CURRENT_USER'
export const TOKEN = 'TOKEN'
export const RESPONSE = 'RESPONSE'
export const LOGOUT = 'LOGOUT'

export const HOMES = 'HOMES'

export const ACCOUNT = 'ACCOUNT'

export const RETRIVE_PLATON = 'RETRIVE_PLATON'
export const LIST_KINDERGARTENS = 'LIST_KINDERGARTENS'
export const KINDERGARTENS_PAYMENT = "KINDERGARTENS_PAYMENT"
