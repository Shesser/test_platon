/**
 * Created by shesser on 15.09.17.
 */
import api from '../../api/users'
import * as types from '../mutations'

/*state this module*/
const state = {
    current_user: {},
    autorizate_user: !!localStorage.getItem("token"),
    token: '',
    response: ''
}

const getters = {
    current_user : state => state.current_user,
    autorizate_user : state => state.autorizate_user,
    token: state => state.token,
    response: sate => state.response
}

const actions = {
    getCurrentUser ( {commit}) {
        api.current_user(response => {
          commit(types.CURRENT_USER, response)
          commit(types.RESPONSE, response)
        }, err => {
          commit(types.RESPONSE, err)
        })
    },

    sign_in({ commit }, form) {
      api.login(response => {
          localStorage.setItem("token", response.token)
          commit(types.AUTORIZATE, true)
          commit(types.TOKEN)
          commit(types.RESPONSE, response)
      }, error => {
          commit(types.RESPONSE, error)
      }, form)
    },

    logout({ commit }) {
      api.logout(response => {
          commit(types.AUTORIZATE, false)
          commit(types.LOGOUT, false)
          commit(types.RESPONSE, response)
          localStorage.clear();
      }, error => {
          console.log(error)
          commit(types.RESPONSE, error)
      })
    },
}

const mutations = {
  [types.LOGOUT] (state) {
      state.current_user = null
      state.token = null
  },
    [types.CURRENT_USER] (state, response) {
        state.current_user = response.current_user
    },
    [types.AUTORIZATE] (state, value) {
        state.autorizate_user = value
    },
    [types.TOKEN] (state) {
        state.token = localStorage.getItem("token")
    },
    [types.RESPONSE] (state, response) {
        state.response = response
    },
}

export default {
    state,
    mutations,
    getters,
    actions
}
