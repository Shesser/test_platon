/**
 * Created by shesser on 15.09.17.
 */
import api from '../../api/homes'
import * as types from '../mutations'

/*state this module*/
const state = {
    homes: []
}

const getters = {
    homes : state => state.homes,
}

const actions = {
    getHomes ({commit}) {
        api.homes(response => {
          commit(types.HOMES, response)
          commit(types.RESPONSE, response)
        }), err => {
          commit(types.RESPONSE, err)
        }
    }
}

const mutations = {
  [types.HOMES] (state, response) {
    state.homes = response.homes
  }
}

export default {
    state,
    mutations,
    getters,
    actions
}
