/**
 * Created by shesser on 15.09.17.
 */
import api from '../../api/kindergartens'
import * as types from '../mutations'

/*state this module*/
const state = {
    kindergartens: [],
    kindergartensPayments: []
}

const getters = {
    kindergartens : state => state.kindergartens,
    kindergartensPayments: state => state.kindergartensPayments
}

const actions = {
    getListKindergartens ({commit}) {
        api.kindergartens(response => {
          commit(types.LIST_KINDERGARTENS, response)
          commit(types.RESPONSE, response)
        }), err => {
          commit(types.RESPONSE, err)
        }
    },
    getKindergartenPayments({commit}) {
      api.kindergartensPayments(response => {
        commit(types.KINDERGARTENS_PAYMENT, response)
        commit(types.RESPONSE, response)
      }, err => {
        commit(types.RESPONSE, err)
      })
    }
}

const mutations = {
  [types.LIST_KINDERGARTENS] (state, response) {
    state.kindergartens = response.kindergartens
  },
  [types.KINDERGARTENS_PAYMENT] (state, response) {
    state.kindergartensPayments = response.kindergartens_payments
  }
}

export default {
    state,
    mutations,
    getters,
    actions
}
