/**
 * Created by shesser on 15.09.17.
 */
import api from '../../api/account'
import * as types from '../mutations'

/*state this module*/
const state = {
    account: null
}

const getters = {
    account : state => state.account,
}

const actions = {
    getAccountForPayment ({commit}, home_id) {
        api.getAccount(response => {
          commit(types.ACCOUNT, response)
          commit(types.RESPONSE, response)
        }, err => {
          commit(types.RESPONSE, err)
        }, home_id)
    }
}

const mutations = {
  [types.ACCOUNT] (state, response) {
    state.account = response.result
  }
}

export default {
    state,
    mutations,
    getters,
    actions
}
