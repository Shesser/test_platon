/**
 * Created by shesser on 15.09.17.
 */
import api from '../../api/payment'
import * as types from '../mutations'

/*state this module*/
const state = {
   platon: null
}

const getters = {
    platon : state => state.platon
}

const actions = {
    sendPayment ( {commit}, data ) {
        api.payments(response => {
          commit(types.RETRIVE_PLATON, response.params)
          commit(types.RESPONSE, response)
        }, err => {
          console.log(err);
        }, data )
    }
}

const mutations = {
  [types.RETRIVE_PLATON] (state, platon) {
      state.platon = platon
  },
}

export default {
    state,
    mutations,
    getters,
    actions
}
